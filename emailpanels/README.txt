Panels Extras: Email CCK Field and Panels 3 Integration

DESCRIPTION
-----------

Email CCK Field and Panels 3 Integration exposes the CCK Email (http://drupal.org/project/email) field to Panels. The field can be formatted as a clickable email address or a link to a contact form.

REQUIREMENTS
------------

Panels Extras requires Panels and CTools. Email CCK Field and Panels 3 Integration requires the Email module to be installed also.

INSTALLATION
------------

1. Copy the entire panels_extras directory to Drupal's sites/all/modules directory.
2. Login as an administrator and enable the Email CCK Field and Panels 3 Integration module listed in the Panels Extras section.

USAGE
-----

1. From the Panels content editor add the "Field: Email (field_email) - Email" content from the Node category
2. The configuration fro this content provides two formatter options: 1) A simple mailto: link of the address entered int he field, 2) Link to a contact form that will send to the address provided in the field.
3. The contact form option requires that the display setting of the Email field is set to Email contact form in the Display Settings of the content type.